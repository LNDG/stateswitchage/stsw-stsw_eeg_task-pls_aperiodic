[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## PLS of excitability signatures

This includes multiple task pls variants that are run across multiple excitability signatures.

**a01_taskPLS_mse_fooof_z**

- *results reported in supplement*
- joint pls including both sample entropy (avg. durign final 2.5 s) and aperiodic slopes; each z-transformed prior to inclusion in model

![image](figures/a01_pls_rcp.png)
![image](figures/a01_pls_rcp_lv2.png)
![image](figures/a01_pls_topo_1f.png)
![image](figures/a01_pls_topo_1f_lv2.png)

**b01_taskPLS_mse_fooof_1_24_z**

- same as above, but restricted to frontal channels (test wheteher frontal channels are sufficient for effects)

**m01_taskPLS_mse_fooof_alpha_51_60**

- pls including 1/f fooof estimates, sample entropy and alpha power (averaged between 6 and 15 Hz)
- all signatures were calculated and averaged across the final 2.5 s of stimulus presentation

**m01_taskPLS_mse_fooof_alpha_51_60_plotLV1**

Loadings on the three signatures:
![image](figures/m01_pls_topo_all.png)

Brainscores
![image](figures/m01_pls_rcp.png)