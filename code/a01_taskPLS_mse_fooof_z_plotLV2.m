% Create an overview plot featuring the results of the multivariate PLS
% comparing spectral changes during the stimulus period under load

clear all; cla; clc; restoredefaultpath;

saveplot = 0;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, '..', 'mse', 'tools');
    addpath(pn.tools) % requires mysigstar_vert!
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(genpath(fullfile(pn.tools, 'barwitherr')))
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')))
    addpath(genpath(fullfile(pn.tools, 'BrewerMap')))
    addpath(genpath(fullfile(pn.tools, 'Cookdist')))

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

load(fullfile(pn.data, 'a01_taskPLS_mse_fooof.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')
load(fullfile(pn.data,'elec.mat'))

stat.clusters.prob

indLV =2;
lvdat = reshape(result.boot_result.compare_u(:,indLV), ...
    size(stat.prob,1), size(stat.prob,2), size(stat.prob,3));
stat.prob = lvdat;
stat.mask = lvdat > 0 | lvdat < -0;

%% invert solution

% stat.mask = stat.mask;
% stat.prob = stat.prob.*-1;
% result.vsc = result.vsc.*-1;
% result.usc = result.usc.*-1;

%% plot multivariate brainscores

maskNaN = double(stat.mask);
maskNaN(maskNaN==0) = NaN;

%% plot multivariate topographies

h = figure('units','normalized','position',[.1 .1 .2 .2]);
cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';
plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
cfg.figure = h;
cfg.zlim = [-2 2]; 
plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,1,:).*stat.prob(:,1,:),3),2));
plotData.powspctrm = cat(1, plotData.powspctrm, zeros(60-size(stat.prob,1),1));
ft_topoplotER(cfg,plotData);
cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');
title('sampen')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['a01_pls_topo_sampen_lv2'];
if saveplot == 1
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end
    
h = figure('units','normalized','position',[.1 .1 .2 .2]);
cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';
plotData = [];
plotData.label = elec.label; % {1 x N}
plotData.dimord = 'chan';
cfg.figure = h;
cfg.zlim = [-5 5]; 
plotData.powspctrm = squeeze(nanmean(nanmean(stat.mask(:,2,:).*stat.prob(:,2,:),3),2));
plotData.powspctrm = cat(1, plotData.powspctrm, zeros(60-size(stat.prob,1),1));
ft_topoplotER(cfg,plotData);
cb = colorbar('location', 'EastOutside'); set(get(cb,'ylabel'),'string','Mean BSR');
title('1/f')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['a01_pls_topo_1f_lv2'];
if saveplot == 1
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
end

%% plot using raincloud plot

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;
    conds = {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'};
    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,indLV);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,indLV);
        end
    end
    
%% plot RainCloudPlot (within-subject centered)

    cBrew(1,:) = 2.*[.3 .1 .1];
    cBrew(2,:) = [.6 .6 .6];

    idx_outlier = cell(1); idx_standard = cell(1);
    for indGroup = 1:2
        dataToPlot = uData{indGroup}';
        % define outlier as lin. modulation that is more than three scaled median absolute deviations (MAD) away from the median
        X = [1 1; 1 2; 1 3; 1 4]; b=X\dataToPlot'; IndividualSlopes = b(2,:);
        outliers = isoutlier(IndividualSlopes, 'median');
        idx_outlier{indGroup} = find(outliers);
        idx_standard{indGroup} = find(outliers==0);
    end


    h = figure('units','centimeter','position',[0 0 25 10]);
    for indGroup = 1:2
        dataToPlot = uData{indGroup}';
        % read into cell array of the appropriate dimensions
        data = []; data_ws = [];
        for i = 1:4
            for j = 1:1
                data{i, j} = dataToPlot(:,i);
                % individually demean for within-subject visualization
                data_ws{i, j} = dataToPlot(:,i)-...
                    nanmean(dataToPlot(:,:),2)+...
                    repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
                data_nooutlier{i, j} = data{i, j};
                data_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                data_ws_nooutlier{i, j} = data_ws{i, j};
                data_ws_nooutlier{i, j}(idx_outlier{indGroup}) = [];
                % sort outliers to back in original data for improved plot overlap
                data_ws{i, j} = [data_ws{i, j}(idx_standard{indGroup}); data_ws{i, j}(idx_outlier{indGroup})];
            end
        end

        % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

        subplot(1,2,indGroup);
        set(gcf,'renderer','Painters')
            cla;
            cl = cBrew(indGroup,:);
            rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
            h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
            view([90 -90]);
            axis ij
        box(gca,'off')
        set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
        yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./2, yticks(4)+(yticks(2)-yticks(1))./2]);

        minmax = [min(min(cat(2,data_ws{:}))), max(max(cat(2,data_ws{:})))];
        xlim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
        ylabel('Target load'); xlabel({'Brainscore'; '[Individually centered]'})

        % test linear effect
        curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
        X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
        [~, p, ci, stats] = ttest(IndividualSlopes);
        title(['M:', num2str(round(mean(IndividualSlopes),3)), '; p=', num2str(round(p,3))])
        
        % also test quadratic fit
        for indID = 1:size(curData,1)
            tmp=polyfit([1,2,3,4], curData(indID,:), 1);
            lin(indID,1) = tmp(1); lin(indID,2) = tmp(2);
            tmp=polyfit([1,2,3,4], curData(indID,:), 2);
            quad(indID,1) = tmp(1); quad(indID,2) = tmp(2); quad(indID,3) = tmp(3);
        end
        [h,p_q]= ttest(quad(:,3));
        [h,p_ql]= ttest(quad(:,2));
        [h,p_l]= ttest(lin(:,1));
        disp(['group ', num2str(indGroup), ': p quad: ', num2str(round(p_q,3)), ...
            ', p quad-lin: ', num2str(round(p_ql,3)), ', p lin: ', num2str(round(p_l,3))])
        
        % fit quadratic mixed effects model using fitlme
        Subject = double(repmat([1:size(curData,1)],1,4)); Subject = Subject(:);
        brainscore = double(curData(:));
        load = double(repmat([1:4], size(curData,1),1)); load = load(:);
        Data = table(brainscore, load, Subject);
        quadmdl{indGroup} = fitlme(Data,'brainscore~load^2+(1+load^2|Subject)');

    end
	set(findall(gcf,'-property','FontSize'),'FontSize',18)
	figureName = ['a01_pls_rcp_lv2'];
    if saveplot == 1
        saveas(h, fullfile(pn.figures, figureName), 'epsc');
        saveas(h, fullfile(pn.figures, figureName), 'png');
    end
    
    % check quadratic effects
    quadmdl{1}
    quadmdl{2}
        
%% save individual brainscores & lin. modulation

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

LV1.data = cat(1,uData{1}',uData{2}');
X = [1 1; 1 2; 1 3; 1 4]; b=X\LV1.data'; 
LV1.linear = b(2,:);
LV1.IDs = IDs;

%save(fullfile(pn.data, 'n01_LV1.mat'), 'LV1')