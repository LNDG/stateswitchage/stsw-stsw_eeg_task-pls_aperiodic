% Set up EEG PLS for a task PLS using sample entropy

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, '..', 'mse', 'data');
pn.data_mse    = fullfile(rootpath, '..', 'mse', 'data');
pn.data_tfr    = fullfile(rootpath, '..', 'tfr', 'data');
pn.data_out     = fullfile(rootpath, 'data');
pn.tools        = fullfile(rootpath, '..', 'mse', 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
pn.slopes       = fullfile(rootpath, '..', 'aperiodic', 'data');
pn.fooof        = fullfile(rootpath, '..', 'aperiodic_fooof', 'data');
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');
pn.data_pupil = fullfile(rootpath, '..', '..', 'stsw_eye', 'pupil', 'data');

%% add seed for reproducibility

rng(0, 'twister');

%% load data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

%% regularize solution by resticting to frontal channels

chans = 1:24;

%% add sampen

SampEn = load(fullfile(pn.data_mse, 'mmse_output', 'mseavg.mat'), 'mseavg');

idx_YA = find(ismember(SampEn.mseavg.IDs, IDs(ageIdx{1})));
idx_OA = find(ismember(SampEn.mseavg.IDs, IDs(ageIdx{2})));

idxTime = SampEn.mseavg.time >= 3.5 & SampEn.mseavg.time < 6;
% only include first scale
% SE{1} = SampEn;
% SE{1}.mseavg.dat = nanmean(SampEn.mseavg.dat(idx_YA,chans,1,idxTime,:),4);
% SE{2} = SampEn;
% SE{2}.mseavg.dat = nanmean(SampEn.mseavg.dat(idx_OA,chans,1,idxTime,:),4);

SE{1} = SampEn;
SE{1}.mseavg.dat = SampEn.mseavg.dat(idx_YA,chans,1,idxTime,:);
SE{2} = SampEn;
SE{2}.mseavg.dat = SampEn.mseavg.dat(idx_OA,chans,1,idxTime,:);

% add 1/f slopes
curData = fullfile(pn.fooof, 'foof_exponents.mat');
load(curData, 'fooof_exponents');
load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'), 'STSWD_summary')
idx_YA_summary = ismember(STSWD_summary.IDs, IDs(ageIdx{1}));
idx_OA_summary = ismember(STSWD_summary.IDs, IDs(ageIdx{2}));

idx_YA = find(ismember(SampEn.mseavg.IDs, IDs(ageIdx{1})));
idx_OA = find(ismember(SampEn.mseavg.IDs, IDs(ageIdx{2})));

slopes_YA = fooof_exponents(idx_YA_summary,:,chans);
slopes_OA = fooof_exponents(idx_OA_summary,:,chans);

% append slopes values to frequency dimension
slopesYA = repmat(permute(shiftdim(slopes_YA,-2),[3,5,1,2,4]),1,1,1,size(SE{1}.mseavg.dat,4),1);
%slopesYA(:,:,:,2:end,:) = NaN; % replace repmat with NaNs to avoid skewing the solution
slopesOA = repmat(permute(shiftdim(slopes_OA,-2),[3,5,1,2,4]),1,1,1,size(SE{1}.mseavg.dat,4),1);
%slopesOA(:,:,:,2:end,:) = NaN; % replace repmat with NaNs to avoid skewing the solution
SE{1}.mseavg.dat = cat(3, SE{1}.mseavg.dat, slopesYA);
SE{2}.mseavg.dat = cat(3, SE{2}.mseavg.dat, slopesOA);

TFRdata{1} = zscore(SE{1}.mseavg.dat,[],5);
TFRdata{2} = zscore(SE{2}.mseavg.dat,[],5);

%% build models

% create datamat

num_chans = size(TFRdata{1},2);
num_freqs = size(TFRdata{1},3);
num_time = size(TFRdata{1},4);

num_subj_lst = [numel(idx_YA), numel(idx_OA)];
num_cond = 4;
num_grp = 2;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup};
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;

save(fullfile(pn.data_out, 'b01_taskPLS_mse_fooof.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')
