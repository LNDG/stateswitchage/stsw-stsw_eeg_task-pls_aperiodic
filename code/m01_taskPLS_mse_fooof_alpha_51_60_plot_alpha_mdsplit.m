clear all; cla; clc; restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data');

%load(fullfile(pn.data, 'm01_LV1.mat'), 'LV1')
load('/Users/kosciessa/Desktop/tardis/LNDG/stateswitch/stsw_eeg_task/tfr/data/c01_LV1.mat')

pn.data_tfr    = fullfile(rootpath, '..', 'tfr', 'data');
pn.tools        = fullfile(rootpath, '..', 'mse', 'tools');
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'shadedErrorBar'));

%% load data

IDs = LV1.IDs;

ageIdx{1} = find(cellfun(@str2num, IDs, 'un', 1)<2000);
ageIdx{2} = find(cellfun(@str2num, IDs, 'un', 1)>2000);

%TFRdata_Wavelet = NaN(100,60,4);
TFRdata_Wavelet = [];
for indID = 1:numel(IDs)
    disp(['Processing ID', IDs{indID}])
    % load wavelet results
    tmp = load(fullfile(pn.data_tfr, 'tfrPowBySub', [IDs{indID}, '_tfr.mat']), 'load_log10');
    for indCond = 1:4
        % restrict to the same time points
        timeIdx_Wavelet = find(tmp.load_log10.time >= 2.5 & tmp.load_log10.time < 6.5);
        TFRdata_Wavelet(indID,:,:,:,indCond) = tmp.load_log10.powspctrm...
            (indCond,:,tmp.load_log10.freq>7 & tmp.load_log10.freq<14,timeIdx_Wavelet);
    end
end

time = tmp.load_log10.time(timeIdx_Wavelet)-3;

alphaMerged = permute(squeeze(nanmean(nanmean(TFRdata_Wavelet(:,51:60, :, :,:),3),2)),[1,3,2]);

colGrey = brewermap(4,'Greys');
colBlue = brewermap(4,'Reds');

%% md split on LV1: YA

[~, sortInd_LV1] = sort(LV1.linear(ageIdx{1}),'ascend');
sortInd_LV1_bottom = sortInd_LV1(1:ceil(numel(sortInd_LV1)/2));
sortInd_LV1_top = sortInd_LV1(ceil(numel(sortInd_LV1)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    curIDs = ageIdx{1}(sortInd_LV1_top);
    grandAverage = squeeze(nanmean(alphaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(alphaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('Time (ms); response-locked')
    xlim([0 3]); %ylim([-.03 .2])
    ylabel({'alpha power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 top');
    
subplot(1,2,2);
    cla; hold on;
    curIDs = ageIdx{1}(sortInd_LV1_bottom);
    grandAverage = squeeze(nanmean(alphaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(alphaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([0 3]); %ylim([-.03 .2])
    ylabel({'alpha power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 bottom');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% md split on LV1: OA

[~, sortInd_LV1] = sort(LV1.linear(ageIdx{2}),'ascend');
sortInd_LV1_bottom = sortInd_LV1(1:ceil(numel(sortInd_LV1)/2));
sortInd_LV1_top = sortInd_LV1(ceil(numel(sortInd_LV1)/2)+1:end);

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cla; hold on;
    curIDs = ageIdx{2}(sortInd_LV1_top);
    grandAverage = squeeze(nanmean(alphaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(alphaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    legend([l1.mainLine, l4.mainLine], {'Load 1'; 'Load 4'}, 'location', 'NorthWest'); legend('boxoff')
    xlabel('Time (ms); response-locked')
    xlim([0 3]); %ylim([-.03 .2])
    ylabel({'alpha power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 top');
    
subplot(1,2,2);
    cla; hold on;
    curIDs = ageIdx{2}(sortInd_LV1_bottom);
    grandAverage = squeeze(nanmean(alphaMerged(curIDs,1:4,:,:),2));
    curData = squeeze(nanmean(alphaMerged(curIDs,1,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(1,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,2,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(2,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,3,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(3,:),'linewidth', 2}, 'patchSaturation', .1);
    
    curData = squeeze(nanmean(alphaMerged(curIDs,4,:,:),2));
    curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', colBlue(4,:),'linewidth', 2}, 'patchSaturation', .1);
    
    xlabel('Time (ms); response-locked')
    xlim([0 3]); %ylim([-.03 .2])
    ylabel({'alpha power';'(a.u.; normalized)'});
    xlabel({'Time (s)'});
    title('LV1 bottom');
set(findall(gcf,'-property','FontSize'),'FontSize',18)